<?php
require_once 'advertisements.php';

/*Faile skelbimai.php yra pateikti įvairūs skelbimai.
Padarykite puslapį kuriame būtų tvarkingai atvaizduoti visi šie skelbimai.
Puslapio apačioje turėtų rašyti:
◦ Kiek išviso yra skelbimų
◦ Kiek skelbimų yra apmokėtų
◦ Kokia suma yra gauta už skelbimus (suma kainos tų skelbimų kurie yra apmokėti)
◦ Kokia suma dar turėtų būti apmokėta
*/?>

<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>

<form>
    <table class="table table-striped" border=1>
        <tr>           
            <th>Id</th>
            <th>Skelbimas</th>
            <th>Kaina</th>
            <th>Apmokėjimas</th>
        </tr>
        <?php foreach ($advertisements as $advertisement) {?>
        <tr>
            <td>
                <?php echo $advertisement['id']; ?>
            </td>
            <td>
                <?php echo $advertisement['text']; ?>
            </td>
            <td>
                <?php echo $advertisement['cost']; ?>
            </td>
            <td>
                <?php 
                if ($advertisement['onPay'] != 0) {
                    echo date("Y-m-d", $advertisement['onPay']);                                    
                } else {
                    echo $advertisement['onPay'];
                } ?>
            </td>        
        </tr>
        <?php }?>
    </table>   
</form>
</body>
</html>

<?php
$countOfPayedAds = 0;
$sumOfPayedAds = 0;
$sumNotPayed = 0;
foreach ($advertisements as $advertisement) {
    if ($advertisement['onPay'] != 0) {
        $countOfPayedAds++;
        $sumOfPayedAds = $sumOfPayedAds + $advertisement['cost'];
    } else {
        $sumNotPayed = $sumNotPayed + $advertisement['cost'];
    }
}
echo 'Iš viso skelbimų: ' . count($advertisements). '<br>'; 
echo 'Apmokėtų skelbimų: ' . $countOfPayedAds . '<br>'; 
echo 'Gauta suma už apmokėtus skelbimus: ' . $sumOfPayedAds . '<br>'; 
echo 'Už skelbimus likusi apmokėti suma: ' . $sumNotPayed . '<br>'; 
?>
